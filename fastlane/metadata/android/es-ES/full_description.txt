<p>Aplicación de Android (beta) que permite congelar por completo todas las actividades en segundo plano de cualquier aplicación.</p>
<ul>
<li>Recupera el control sobre lo que se ejecuta en tu teléfono</li>
<li>Mejore la duración de la batería y reduzca el uso de datos móviles al congelar las aplicaciones que se usan con poca frecuencia</li>
<li>Especialmente útil durante un recorrido, donde solo necesita algunas aplicaciones pero una batería de larga duración</li>
</ul>
<p>Greenify también puede hacer esto, pero no es de código abierto.</p>
<p>SuperFreezZ no es otro administrador de tareas que promete eliminar 10 GB de datos por mes o hacer que su dispositivo sea el doble de rápido. Esto es imposible.</p>
<p>En cambio, SuperFreezZ es honesto acerca de sus desventajas: congelar las aplicaciones de uso diario probablemente agote la batería un poco más rápido. Además, estas aplicaciones tardarán más en iniciarse la próxima vez que las use: SuperFreezZ supercongelará sus aplicaciones, se tarda entre 1 y 3 segundos en descongelarlas. Greenify tiene las mismas desventajas, excepto que el autor de Greenify no te advierte al respecto. Así que: no te excedas y SuperFreezZ te resultará muy útil.</p>
<p>Ejemplos de aplicaciones que merecen ser congeladas:</p>
<ul>
<li>Aplicaciones que no son de confianza (que no desea ejecutar en segundo plano)</li>
<li>Aplicaciones que rara vez usas</li>
<li>Aplicaciones molestas</li>
</ul>
<h2 id="features">Características</h2>
<ul>
<li>Opcionalmente, funciona sin el servicio de accesibilidad, ya que esto ralentiza el dispositivo</li>
<li>Puede congelar solo las aplicaciones que no se usan durante una semana (se puede configurar)</li>
<li>Elija una lista blanca (congele todo por estándar) o una lista negra (no congele nada por estándar)</li>
<li>Puede congelar aplicaciones cuando la pantalla se apaga</li>
<li>Opciones para congelar aplicaciones del sistema e incluso SuperFreezZ</li>
<li>Software totalmente gratuito y de código abierto</li>
</ul>
<h2 id="contributing-to-superfreezz">Contribuyendo SuperFreezZ</h2>
<h3 id="development">Desarrollo</h3>
<p>Si tiene un problema, una pregunta o una idea, ¡simplemente abra un problema!</p>
<p>Si desea ayudar con el desarrollo, eche un vistazo a los problemas o piense en algo que podría mejorarse y abra un problema para ello.</p>
<p>Por favor, dime qué vas a hacer, para evitar implementar lo mismo al mismo tiempo :-)</p>
<h3 id="translate">Traducir</h3>
<p>Puede <a href="https://hosted.weblate.org/engage/superfreezz/">traducir SuperFreezZ en Weblate</a>.</p>
<h2 id="credits">Créditos</h2>
<p>El código para mostrar la lista de aplicaciones es de <a href="https://f-droid.org/wiki/page/axp.tool.apkextractor">ApkExtractor</a>.</p>
<p>Robin Naumann hizo una buena introducción. La introducción se creó utilizando la biblioteca AppIntro.</p>
<p>El fondo gráfico de la función es de aquí: https://pixabay.com/photos/thunder-lighting-lightning-cloud-1368797/, el texto se agregó con https://www.norio.be/android-feature -generador-gráfico/.</p>
<h2 id="qa">P/R</h2>
<p>P/R:</p>
<p>P: ¿Cuál es la diferencia entre hibernar y congelar? R: No hay ninguno en absoluto. Si hibernas una aplicación con Greenify, incluso se mostrará congelada en SuperFreezZ y viceversa.</p>
<p>P: ¡Pero la ortografía correcta sería "SuperFreeze"! R: Lo sé.</p>
<p>P: ¿Tiene alguna intención de vender congeladores? R: No.</p>
