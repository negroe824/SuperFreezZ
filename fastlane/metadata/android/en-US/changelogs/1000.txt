Update translations
Remove annoying message that was sometimes shown on startup erreneously
Fix OxygenOS 12 freezing issue
Fix fragment_modes_tab
Remove Crowdin
Fix "parentNode.getChild(i) must not be null" crash
Update list of F-Droid apps
