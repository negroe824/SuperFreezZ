WSPANIAŁE nowe intro, dziękujemy Robin!!

Pierwsza wersja beta

Inne zmiany:
Zaktualizowano tłumaczenia
Zmieniono nazwę trybu zamrażania „Auto” na „Inteligentny” (#16)
Dotknięcie aplikacji na liście spowoduje jej zamrożenie. Nie próbuj tego, jeśli jest już zamrożona.
Możliwość ponownego wyświetlenia intro w ustawieniach
Dodano porządne raportowanie o awariach
Inne drobne ulepszenia i poprawki błędów
