SF mit älteren Android-Versionen kompatibel gemacht
#14 behoben: beim händischen Einfrieren "Stop erzwingen", "OK" und "Zurück" auffordern
Neue Screenshots
Übersetzungen
SuperFreezZ nicht selbst automatisch einfrieren
App erst nach mind. 2 Sek. Nutzung als 'verwendet' ansehen
Erkennen von 'kürzlich verwendet' repariert
Schaltfläche 'AutoFreeze' ausblenden wenn usagestats-Zugriff nicht gewährt wurde
Aktualisierungen
Verbesserte Benutzerdienst-Auswahl ("Einfrieren von Apps automatisieren?")
